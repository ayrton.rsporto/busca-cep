package br.com.metasix.cep_requisicao;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    public  static final String TITULO = "Busca Cep";
    private EditText campoCep;
    private TextView respostaRua;
    private TextView respostaComplemento;
    private TextView respostaBairro;
    private TextView respostaLocalidade;
    private TextView respostaUf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTitle(TITULO);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        campoCep = findViewById(R.id.activity_CEP);
        respostaRua = findViewById(R.id.activity_retorna_rua);
        respostaComplemento = findViewById(R.id.activity_retorna_complemento);
        respostaBairro = findViewById(R.id.activity_retorna_bairro);
        respostaLocalidade = findViewById(R.id.activity_retorna_localidade);
        respostaUf = findViewById(R.id.activity_retorna_uf);

        Button botao = findViewById(R.id.activity_botao_localizar);
        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (campoCep.getText().toString().length() > 0 &&
                        !campoCep.getText().toString().equals("") &&
                        campoCep.getText().toString().length() == 8) {


                    getServiceCep();

                }else{
                    Toast.makeText(MainActivity.this, "CEP Invalido", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getServiceCep(){
        Call<CEP> call = new RetrofitConfig().getCEPService().buscarCEP(campoCep.getText().toString());
        call.enqueue(new Callback<CEP>() {
            @Override
            public void onResponse(Call<CEP> call, Response<CEP> response) {
                CEP cep = response.body();
                preencheCampos(cep);
            }

            @Override
            public void onFailure(Call<CEP> call, Throwable t) {

                Toast.makeText(MainActivity.this, "erro" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void preencheCampos(CEP cep) {
        respostaRua.setText(cep.getLogradouro());
        respostaComplemento.setText(cep.getComplemento());
        respostaBairro.setText(cep.getBairro());
        respostaLocalidade.setText(cep.getLocalidade());
        respostaUf.setText(cep.getUf());
    }

}
